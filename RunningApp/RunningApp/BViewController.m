//
//  BViewController.m
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/10/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"
#import "BViewController.h"
#import "MenuViewController.h"
#import "LViewController.h"

@interface BViewController ()

@end

@implementation BViewController

int protein;
int fat;
int carbs;
int totalCalories;


@synthesize button1, button2, button3, button4, button5, button6, button7, button8, button9;

@synthesize overallCaloriesBVC, overallCarbsBVC, overallFatBVC, overallProBVC;


- (void)viewDidLoad {
    button1.layer.cornerRadius = 10; // this value vary as per your desire
    button1.clipsToBounds = YES;
    [[button1 layer] setBorderWidth:2.0f];
    [[button1 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button2.layer.cornerRadius = 10; // this value vary as per your desire
    button2.clipsToBounds = YES;
    [[button2 layer] setBorderWidth:2.0f];
    [[button2 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button3.layer.cornerRadius = 10; // this value vary as per your desire
    button3.clipsToBounds = YES;
    [[button3 layer] setBorderWidth:2.0f];
    [[button3 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button4.layer.cornerRadius = 10; // this value vary as per your desire
    button4.clipsToBounds = YES;
    [[button4 layer] setBorderWidth:2.0f];
    [[button4 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button5.layer.cornerRadius = 10; // this value vary as per your desire
    button5.clipsToBounds = YES;
    [[button5 layer] setBorderWidth:2.0f];
    [[button5 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button6.layer.cornerRadius = 10; // this value vary as per your desire
    button6.clipsToBounds = YES;
    [[button6 layer] setBorderWidth:2.0f];
    [[button6 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button7.layer.cornerRadius = 10; // this value vary as per your desire
    button7.clipsToBounds = YES;
    [[button7 layer] setBorderWidth:2.0f];
    [[button7 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button8.layer.cornerRadius = 10; // this value vary as per your desire
    button8.clipsToBounds = YES;
    [[button8 layer] setBorderWidth:2.0f];
    [[button8 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button9.layer.cornerRadius = 10; // this value vary as per your desire
    button9.clipsToBounds = YES;
    [[button9 layer] setBorderWidth:2.0f];
    [[button9 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)eggsButton:(id)sender {
    protein +=18;
    fat += 15;
    carbs += 2;
    totalCalories += 222;
    [button1 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)yogurtButton:(id)sender {
    protein +=15;
    fat += 0;
    carbs += 15;
    totalCalories += 120;
     [button2 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)proteinShakeButton:(id)sender {
    protein +=30;
    fat += 3;
    carbs += 6;
    totalCalories += 170;
     [button3 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)oatmealButton:(id)sender {
    protein +=6;
    fat += 3;
    carbs += 28;
    totalCalories += 160;
    [button4 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)bagelButton:(id)sender {
    protein += 12;
    fat += 4;
    carbs += 63;
    totalCalories += 337;
     [button5 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)fruitButton:(id)sender {
    protein +=1;
    fat += 5;
    carbs += 2;
    totalCalories += 50;
    [button6 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)ccButton:(id)sender {
    protein +=1;
    fat += 5;
    carbs += 2;
    totalCalories += 50;
    [button7 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)pbButton:(id)sender {
    protein +=8;
    fat += 16;
    carbs += 6;
    totalCalories += 200;
    [button8 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)almondButton:(id)sender {
    protein +=6;
    fat += 14;
    carbs += 6;
    totalCalories += 162;
   [button9 setBackgroundColor:[UIColor grayColor]];
}

/*- (IBAction)addMealButton:(id)sender {
  NSString *proteinStr = proteinTextbox.text;
  NSInteger proteinInt = [proteinStr integerValue];
  protein += proteinInt;
  
  NSString *carbsStr = carbsTextbox.text;
  NSInteger carbsInt = [carbsStr integerValue];
  carbs += carbsInt;
  
  NSString *fatStr = fatsTextbox.text;
  NSInteger fatInt = [fatStr integerValue];
  fat += fatInt;
  
  totalCalories += (proteinInt * 4) + (carbsInt *4) + (fatInt * 9);
  
}*/


-(void) prepareForSegue:(UIStoryboardSegue *) segue sender:(id)sender {
    LViewController *lvc;
    lvc = [segue destinationViewController];
    
    //Pass the breakfast data to the LVC
    lvc.breakfastProteinLVC = protein;
    lvc.breakfastCarbsLVC = carbs;
    lvc.breakfastFatLVC = fat;
    lvc.breakfastTotalCaloriesLVC = totalCalories;
    
    //Pass the goal values to the LVC
    lvc.overallCarbsLVC = overallCarbsBVC;
    lvc.overallFatLVC = overallFatBVC;
    lvc.overallProLVC = overallProBVC;
    lvc.overallCaloriesLVC = overallCaloriesBVC;
    
    
}

- (IBAction)addLunchButton:(id)sender {

}


@end
