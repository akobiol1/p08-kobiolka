//
//  BViewController.h
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/10/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "MenuViewController.h"
#import "LViewController.h"



@interface BViewController : UIViewController

- (IBAction)eggsButton:(id)sender;
- (IBAction)yogurtButton:(id)sender;
- (IBAction)proteinShakeButton:(id)sender;
- (IBAction)oatmealButton:(id)sender;
- (IBAction)bagelButton:(id)sender;
- (IBAction)fruitButton:(id)sender;
- (IBAction)ccButton:(id)sender;
- (IBAction)pbButton:(id)sender;
- (IBAction)almondButton:(id)sender;
- (IBAction)addLunchButton:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet UIButton *button2;
@property (strong, nonatomic) IBOutlet UIButton *button3;
@property (strong, nonatomic) IBOutlet UIButton *button4;
@property (strong, nonatomic) IBOutlet UIButton *button5;
@property (strong, nonatomic) IBOutlet UIButton *button6;
@property (strong, nonatomic) IBOutlet UIButton *button7;
@property (strong, nonatomic) IBOutlet UIButton *button8;
@property (strong, nonatomic) IBOutlet UIButton *button9;

//Used for passing goal information
@property int overallCaloriesBVC;
@property int overallProBVC;
@property int overallFatBVC;
@property int overallCarbsBVC;

@end

