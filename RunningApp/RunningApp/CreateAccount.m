//
//  CreateAccount.m
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/2/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"
#import "CreateAccount.h"
#import "MapRunViewController.h"
#import "GoalViewController.h"

@interface CreateAccount ()

@end

@implementation CreateAccount


@synthesize firstname, lastname, age, height, weight, gender;

- (void)viewDidLoad {
    
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"greenBackground.jpg"]];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)submitAccount:(id)sender {

}

//Send gender and firstname to Goal ViewController scene
-(void) prepareForSegue:(UIStoryboardSegue *) segue sender:(id)sender {
    GoalViewController *gVC;
    gVC = [segue destinationViewController];
    gVC.firstname = firstname.text;
    if ([gender.text  isEqual: @"female"] || [gender.text  isEqual: @"Female"] || [gender.text  isEqual: @"F"]) {
        gVC.female = true;
    }
    


}
- (IBAction)Next:(id)sender {
}
@end
