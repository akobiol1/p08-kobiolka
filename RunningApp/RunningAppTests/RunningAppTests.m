//
//  RunningAppTests.m
//  RunningAppTests
//
//  Created by Amanda Kobiolka on 5/2/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface RunningAppTests : XCTestCase

@end

@implementation RunningAppTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
