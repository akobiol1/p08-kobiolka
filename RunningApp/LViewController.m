//
//  LViewController.m
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/10/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"
#import "LViewController.h"
#import "BViewController.h"
#import "MenuViewController.h"
#import "DinnerViewController.h"

@interface LViewController ()

@end

@implementation LViewController
int proteinL;
int fatL;
int carbsL;
int totalCaloriesL;


@synthesize button1, button2, button3, button4, button5, button6, button7, button8, button9;

@synthesize overallCaloriesLVC, overallCarbsLVC, overallFatLVC, overallProLVC, breakfastTotalCaloriesLVC, breakfastFatLVC, breakfastCarbsLVC, breakfastProteinLVC;


- (void)viewDidLoad {
    button1.layer.cornerRadius = 10; // this value vary as per your desire
    button1.clipsToBounds = YES;
    [[button1 layer] setBorderWidth:2.0f];
    [[button1 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button2.layer.cornerRadius = 10; // this value vary as per your desire
    button2.clipsToBounds = YES;
    [[button2 layer] setBorderWidth:2.0f];
    [[button2 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button3.layer.cornerRadius = 10; // this value vary as per your desire
    button3.clipsToBounds = YES;
    [[button3 layer] setBorderWidth:2.0f];
    [[button3 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button4.layer.cornerRadius = 10; // this value vary as per your desire
    button4.clipsToBounds = YES;
    [[button4 layer] setBorderWidth:2.0f];
    [[button4 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button5.layer.cornerRadius = 10; // this value vary as per your desire
    button5.clipsToBounds = YES;
    [[button5 layer] setBorderWidth:2.0f];
    [[button5 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button6.layer.cornerRadius = 10; // this value vary as per your desire
    button6.clipsToBounds = YES;
    [[button6 layer] setBorderWidth:2.0f];
    [[button6 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button7.layer.cornerRadius = 10; // this value vary as per your desire
    button7.clipsToBounds = YES;
    [[button7 layer] setBorderWidth:2.0f];
    [[button7 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button8.layer.cornerRadius = 10; // this value vary as per your desire
    button8.clipsToBounds = YES;
    [[button8 layer] setBorderWidth:2.0f];
    [[button8 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button9.layer.cornerRadius = 10; // this value vary as per your desire
    button9.clipsToBounds = YES;
    [[button9 layer] setBorderWidth:2.0f];
    [[button9 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)beanPastaButton:(id)sender {
    proteinL +=10;
    fatL += 8;
    carbsL += 45;
    totalCaloriesL += 292;
    [button1 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)quinoaButton:(id)sender {
    proteinL +=8;
    fatL += 4;
    carbsL += 39;
    totalCaloriesL += 222;
    [button2 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)hummusButton:(id)sender {
    proteinL +=2;
    fatL += 15;
    carbsL += 2;
    totalCaloriesL += 222;
    [button3 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)saladButton:(id)sender {
    proteinL +=18;
    fatL += 5;
    carbsL += 4;
    totalCaloriesL += 71;
    [button4 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)sandwichButton:(id)sender {
    proteinL +=12;
    fatL += 17;
    carbsL += 38;
    totalCaloriesL += 342;
    [button5 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)soupButton:(id)sender {
    proteinL +=5;
    fatL += 2;
    carbsL += 18;
    totalCaloriesL += 110;
    [button6 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)broccoliButton:(id)sender {
    proteinL +=3;
    fatL += 0;
    carbsL += 6;
    totalCaloriesL += 31;
    [button7 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)appleButton:(id)sender {
    proteinL +=0;
    fatL += 0;
    carbsL += 17;
    totalCaloriesL += 65;
    [button8 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)berriesButton:(id)sender {
    proteinL +=1;
    fatL += 15;
    carbsL += 12;
    totalCaloriesL +=49;
    [button9 setBackgroundColor:[UIColor grayColor]];
}

-(void) prepareForSegue:(UIStoryboardSegue *) segue sender:(id)sender {
    DinnerViewController *dvc;
    dvc = [segue destinationViewController];
    
    //Pass the breakfast data to the DVC
    dvc.breakfastProteinDVC = breakfastProteinLVC;
    dvc.breakfastCarbsDVC = breakfastCarbsLVC;
    dvc.breakfastFatDVC = breakfastFatLVC;
    dvc.breakfastTotalCaloriesDVC = breakfastTotalCaloriesLVC;
    
    //Pass the lunch data to the DVC
    dvc.lunchProteinDVC = proteinL;
    dvc.lunchCarbsDVC = carbsL;
    dvc.lunchFatDVC = fatL;
    dvc.lunchTotalCaloriesDVC = totalCaloriesL;
    
    //Pass the goal values to the DVC
    dvc.overallCarbsDVC = overallCarbsLVC;
    dvc.overallFatDVC = overallFatLVC;
    dvc.overallProDVC = overallProLVC;
    dvc.overallCaloriesDVC = overallCaloriesLVC;
    
    
}

- (IBAction)addLunch:(id)sender {
   
}
@end