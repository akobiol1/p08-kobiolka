//
//  ProfileViewController.m
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/6/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"
#import "MenuViewController.h"
#import "ProfileViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ProfileViewController ()

@end



@implementation ProfileViewController
@synthesize proteinLabel, carbsLabel, fatLabel, caloriesLabel,
            p, f, c, tc, femalePVC,
            currentCarbs, currentFat, currentProtein, currentTotalCalories,
            imageView1, imageView2, imageView3,imageView4,
goalCarbs, goalFat, goalProtein, goalTotalCalories;

@synthesize breakfastProtein, breakfastCarbs, breakfastFat, breakfastTotalCalories, lunchCarbs, lunchFat, lunchProtein, lunchTotalCalories, dinnerCarbs, dinnerFat, dinnerProtein, dinnerTotalCalories;


- (void)viewDidLoad {
    
    imageView1.layer.cornerRadius = 10;
    imageView1.clipsToBounds = YES;
    imageView2.layer.cornerRadius = 10;
    imageView2.clipsToBounds = YES;
    imageView3.layer.cornerRadius = 10;
    imageView3.clipsToBounds = YES;
    imageView4.layer.cornerRadius = 10;
    imageView4.clipsToBounds = YES;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(50, 100)];
    [path addLineToPoint:CGPointMake(50, 300)];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [path CGPath];
    shapeLayer.strokeColor = [[UIColor whiteColor] CGColor];
    shapeLayer.lineWidth = 3.5;
    shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    
    [self.view.layer addSublayer:shapeLayer];
    
    UIBezierPath *path1 = [UIBezierPath bezierPath];
    [path1 moveToPoint:CGPointMake(50, 300)];
    [path1 addLineToPoint:CGPointMake(270, 300)];
    
    CAShapeLayer *shapeLayer1 = [CAShapeLayer layer];
    shapeLayer1.path = [path1 CGPath];
    shapeLayer1.strokeColor = [[UIColor whiteColor] CGColor];
    shapeLayer1.lineWidth = 3.5;
    shapeLayer1.fillColor = [[UIColor clearColor] CGColor];
    
    currentCarbs = breakfastCarbs + lunchCarbs + dinnerCarbs;
    currentFat = breakfastFat + lunchFat + dinnerFat;
    currentProtein = breakfastProtein + lunchProtein + dinnerProtein;
    currentTotalCalories = (currentProtein * 4) + (currentFat * 9) + (currentCarbs * 4);
    
    UIView *redBar  = [[UIView alloc] initWithFrame:CGRectMake(35, 50, 10, 10)];
    redBar.backgroundColor = [UIColor redColor];
    [self.view addSubview:redBar];
    
    UIView * yellowBar  = [[UIView alloc] initWithFrame:CGRectMake(135, 50, 10, 10)];
    yellowBar.backgroundColor = [UIColor yellowColor];
    [self.view addSubview:yellowBar];
    
    UIView *greenBar  = [[UIView alloc] initWithFrame:CGRectMake(235, 50, 10, 10)];
    greenBar.backgroundColor = [UIColor greenColor];
    [self.view addSubview:greenBar];
    
    if (currentProtein != 0) {
        int ratio = goalTotalCalories/ 200;
        
        int breakfastSubtract = breakfastTotalCalories/ratio;
        int lunchSubtract = lunchTotalCalories/ratio;
        int dinnerSubtract = dinnerTotalCalories/ratio;
        
        int breakfastY = 300 - breakfastSubtract;
        int lunchY = 300 - lunchSubtract;
        int dinnerY = 300 - dinnerSubtract;
    
        
        UIView *breakfastBar  = [[UIView alloc] initWithFrame:CGRectMake(52, breakfastY, 73, breakfastSubtract)];
        breakfastBar.backgroundColor = [UIColor redColor];
        [self.view addSubview:breakfastBar];
        
        UIView *lunchBar  = [[UIView alloc] initWithFrame:CGRectMake(125, lunchY, 73, lunchSubtract)];
        lunchBar.backgroundColor = [UIColor yellowColor];
        [self.view addSubview:lunchBar];
        
        UIView *dinnerBar  = [[UIView alloc] initWithFrame:CGRectMake(198, dinnerY, 73, dinnerSubtract)];
        dinnerBar.backgroundColor = [UIColor greenColor];
        [self.view addSubview:dinnerBar];
    }
    
    
    NSString * proteinStr=  [NSString stringWithFormat:@"%d / %d", currentProtein, goalProtein];
    proteinLabel.text = proteinStr;
    
    NSString * fatStr=  [NSString stringWithFormat:@"%d / %d", currentFat, goalFat];
    fatLabel.text = fatStr;
    
    NSString * carbsStr=  [NSString stringWithFormat:@"%d / %d", currentCarbs, goalCarbs];
    carbsLabel.text = carbsStr;
    
    NSString * caloriesStr=  [NSString stringWithFormat:@"%d / %d", currentTotalCalories, goalTotalCalories];
    caloriesLabel.text = caloriesStr;
    
    
    
    [self.view.layer addSublayer:shapeLayer1];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)submitAccount:(id)sender {
    
}

-(void) prepareForSegue:(UIStoryboardSegue *) segue sender:(id)sender {
     MenuViewController *mvc;
     mvc = [segue destinationViewController];
    
   
    
    //Send goal values back to main viewcontroller
    mvc.overallProMVC = goalProtein;
    mvc.overallCarbsMVC = goalCarbs;
    mvc.overallCaloriesMVC = goalTotalCalories;
    mvc.overallFatMVC = goalFat;

}
- (IBAction)returnHome:(id)sender {
}
@end