//
//  main.m
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/2/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
