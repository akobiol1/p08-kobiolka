//
//  GoalViewController.m
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/8/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"
#import "BViewController.h"
#import "MenuViewController.h"
#import "LViewController.h"
#import "ProfileViewController.h"
#import "GoalViewController.h"

@interface GoalViewController ()

@end

int option;

@implementation GoalViewController

@synthesize female, firstname, losingWeightB, maintainingWeightB, gainingWeightB, infoTextView, viewProfileButton;

- (IBAction)losingWeightButton:(id)sender {
    option = 1;
    viewProfileButton.hidden = NO;
    [losingWeightB setBackgroundColor:[UIColor grayColor]];
    [maintainingWeightB setBackgroundColor:[UIColor clearColor]];

    [gainingWeightB setBackgroundColor:[UIColor clearColor]];


}

- (IBAction)maintainingWeightButton:(id)sender {
    option = 2;
    viewProfileButton.hidden = NO;
    [maintainingWeightB setBackgroundColor:[UIColor grayColor]];
    
    [losingWeightB setBackgroundColor:[UIColor clearColor]];
    [gainingWeightB setBackgroundColor:[UIColor clearColor]];

}

- (IBAction)gainingWeightButton:(id)sender {
    option = 3;
    viewProfileButton.hidden = NO;
    [gainingWeightB setBackgroundColor:[UIColor grayColor]];
    
    [losingWeightB setBackgroundColor:[UIColor clearColor]];
    [maintainingWeightB setBackgroundColor:[UIColor clearColor]];
    

}


-(void) prepareForSegue:(UIStoryboardSegue *) segue sender:(id)sender {
    ProfileViewController *pvc;
    pvc = [segue destinationViewController];
    
//Losing weight
    if (option == 1) {
        if (female) {
            pvc.goalProtein = 600;
            pvc.goalCarbs = 600;
            pvc.goalFat = 300;
            pvc.goalTotalCalories = 1500;
            pvc.femalePVC = true;
        } else {
            pvc.goalProtein = 800;
            pvc.goalCarbs = 800;
            pvc.goalFat = 400;
            pvc.goalTotalCalories = 2000;
        }
        
//Maintaining weight
    } else if (option == 2) {
        if (female) {
            pvc.goalProtein = 500; //%25
            pvc.goalCarbs = 1100; //%55
            pvc.goalFat = 400; //%20
            pvc.goalTotalCalories = 2000;
            pvc.femalePVC = true;
        } else {
            pvc.goalProtein = 625; //%25
            pvc.goalCarbs = 1375; //%55
            pvc.goalFat = 500; //%20
            pvc.goalTotalCalories = 2500;
            
        }
        
//Gaining weight
    } else {
        if (female) {
            pvc.goalProtein = 1375; //%55
            pvc.goalCarbs = 875; //%35
            pvc.goalFat = 250; //%10
            pvc.goalTotalCalories = 2500;
            pvc.femalePVC = true;

        } else {
            pvc.goalProtein= 1650; //%55
            pvc.goalCarbs = 1050; //%35
            pvc.goalFat = 300; //%10
            pvc.goalTotalCalories = 3000;
        }
        
    }
    

    
}

-(void) viewDidLoad {
    viewProfileButton.hidden = YES;
    [[losingWeightB layer] setBorderWidth:2.0f];
    [[losingWeightB layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    [[maintainingWeightB layer] setBorderWidth:2.0f];
    [[maintainingWeightB layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    [[gainingWeightB layer] setBorderWidth:2.0f];
    [[gainingWeightB layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    [infoTextView setBackgroundColor: [UIColor clearColor]];
    [infoTextView setTextColor: [UIColor whiteColor]];
    [super viewDidLoad];
}





@end
