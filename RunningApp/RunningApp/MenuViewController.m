//
//  MenuViewController.m
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/5/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ViewController.h"
#import "MenuViewController.h"
#import "BViewController.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

@synthesize breakfast, overallCaloriesMVC, overallCarbsMVC, overallFatMVC, overallProMVC, informationTextView;

- (void)viewDidLoad {
    
    [[breakfast layer] setBorderWidth:2.0f];
    [[breakfast layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    [informationTextView setBackgroundColor: [UIColor clearColor]];
    [informationTextView setTextColor: [UIColor whiteColor]];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)submitAccount:(id)sender {
    
}

-(void) prepareForSegue:(UIStoryboardSegue *) segue sender:(id)sender {
    BViewController *bvc;
    bvc = [segue destinationViewController];
    
    //Sending goal values to breakfast controller
    bvc.overallProBVC = overallProMVC;
    bvc.overallCarbsBVC = overallCarbsMVC;
    bvc.overallFatBVC = overallFatMVC;
    bvc.overallCaloriesBVC = overallCaloriesMVC;
}



//Saving values from menu to breakfast
- (IBAction)breakfastAction:(id)sender {
    
   /* BViewController *bVC = [self.storyboard instantiateViewControllerWithIdentifier:@"bvc"];
    [bVC setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:bVC animated:YES completion:nil];
    
    //Sending goal values to breakfast controller
    bVC.overallProBVC = overallProMVC;
    bVC.overallCarbsBVC = overallCarbsMVC;
    bVC.overallFatBVC = overallFatMVC;
    bVC.overallCaloriesBVC = overallCaloriesMVC;*/
}


@end