//
//  MapRunViewController.h
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/4/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapRunViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface MapRunViewController : UIViewController {
    CLLocationManager *locationManager;
}

@property (strong, nonatomic) IBOutlet NSString *firstnameMR;
@property (strong, nonatomic) IBOutlet NSString *lastnameMR;
@property (strong, nonatomic) IBOutlet NSString *ageMR;
@property (strong, nonatomic) IBOutlet NSString *heightMR;
@property (strong, nonatomic) IBOutlet NSString *weightMR;
@property (strong, nonatomic) IBOutlet NSString *genderMR;


@end

