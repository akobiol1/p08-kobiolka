//
//  MapRunViewController.m
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/4/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"
#import "CreateAccount.h"
#import "MapRunViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface MapRunViewController ()

@end

@implementation MapRunViewController

@synthesize firstnameMR, lastnameMR, ageMR, heightMR, weightMR, genderMR;


- (void)viewDidLoad {
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"greenBackground.jpg"]];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {
    locationManager = [[CLLocationManager alloc]init];
    [locationManager requestAlwaysAuthorization];
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
    locationManager.desiredAccuracy= kCLLocationAccuracyNearestTenMeters;
    
    NSLog(@"latitude %lf longitude %lf", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude);
}

@end
