//
//  DinnerViewController.m
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/10/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "ViewController.h"
#import "BViewController.h"
#import "MenuViewController.h"
#import "ProfileViewController.h"
#import "LViewController.h"
#import "DinnerViewController.h"

@interface DinnerViewController ()

@end

@implementation DinnerViewController
int proteinD;
int fatD;
int carbsD;
int totalCaloriesD;

@synthesize button1, button2, button3, button4, button5, button6, button7, button8, button9;
@synthesize breakfastTotalCaloriesDVC, breakfastCarbsDVC, breakfastProteinDVC, breakfastFatDVC, lunchCarbsDVC, lunchFatDVC, lunchProteinDVC, lunchTotalCaloriesDVC, overallCaloriesDVC, overallCarbsDVC, overallFatDVC, overallProDVC;

- (void)viewDidLoad {
    button1.layer.cornerRadius = 10; // this value vary as per your desire
    button1.clipsToBounds = YES;
    [[button1 layer] setBorderWidth:2.0f];
    [[button1 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button2.layer.cornerRadius = 10; // this value vary as per your desire
    button2.clipsToBounds = YES;
    [[button2 layer] setBorderWidth:2.0f];
    [[button2 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button3.layer.cornerRadius = 10; // this value vary as per your desire
    button3.clipsToBounds = YES;
    [[button3 layer] setBorderWidth:2.0f];
    [[button3 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button4.layer.cornerRadius = 10; // this value vary as per your desire
    button4.clipsToBounds = YES;
    [[button4 layer] setBorderWidth:2.0f];
    [[button4 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button5.layer.cornerRadius = 10; // this value vary as per your desire
    button5.clipsToBounds = YES;
    [[button5 layer] setBorderWidth:2.0f];
    [[button5 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button6.layer.cornerRadius = 10; // this value vary as per your desire
    button6.clipsToBounds = YES;
    [[button6 layer] setBorderWidth:2.0f];
    [[button6 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button7.layer.cornerRadius = 10; // this value vary as per your desire
    button7.clipsToBounds = YES;
    [[button7 layer] setBorderWidth:2.0f];
    [[button7 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button8.layer.cornerRadius = 10; // this value vary as per your desire
    button8.clipsToBounds = YES;
    [[button8 layer] setBorderWidth:2.0f];
    [[button8 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    button9.layer.cornerRadius = 10; // this value vary as per your desire
    button9.clipsToBounds = YES;
    [[button9 layer] setBorderWidth:2.0f];
    [[button9 layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)omeletButton:(id)sender {
    proteinD +=18;
    fatD += 21;
    carbsD += 6;
    totalCaloriesD += 282;
    [button1 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)veggieBurgerButton:(id)sender {
    proteinD +=11;
    fatD += 5;
    carbsD += 10;
    totalCaloriesD += 124;
    [button2 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)smoothieButton:(id)sender {
    proteinD +=186;
    fatD += 2;
    carbsD += 60;
    totalCaloriesD += 300;
    [button3 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)dinnerSaladButton:(id)sender {
    proteinD +=5;
    fatD += 10;
    carbsD += 5;
    totalCaloriesD += 151;
    [button4 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)pastaButton:(id)sender {
    proteinD +=8;
    fatD += 2;
    carbsD += 43;
    totalCaloriesD += 220;
    [button5 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)riceButton:(id)sender {
    proteinD +=5;
    fatD += 2;
    carbsD += 44;
    totalCaloriesD += 216;
    [button6 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)subButton:(id)sender {
    proteinD +=8;
    fatD += 2;
    carbsD += 44;
    totalCaloriesD += 230;
    [button7 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)macAndCheeseButton:(id)sender {
    proteinD +=4;
    fatD += 7;
    carbsD += 25;
    totalCaloriesD += 200;
    [button8 setBackgroundColor:[UIColor grayColor]];
}

- (IBAction)fruitButton:(id)sender {
    proteinD +=1;
    fatD += 0;
    carbsD += 25;
    totalCaloriesD += 95;
    [button9 setBackgroundColor:[UIColor grayColor]];
}

-(void) prepareForSegue:(UIStoryboardSegue *) segue sender:(id)sender {
    ProfileViewController *pvc;
    pvc = [segue destinationViewController];
    
    //Pass the breakfast data to the DVC
    pvc.breakfastProtein = breakfastProteinDVC;
    pvc.breakfastCarbs = breakfastCarbsDVC;
    pvc.breakfastFat = breakfastFatDVC;
    pvc.breakfastTotalCalories = breakfastTotalCaloriesDVC;
    
    //Pass the lunch data to the DVC
    pvc.lunchProtein = lunchProteinDVC;
    pvc.lunchCarbs = lunchCarbsDVC;
    pvc.lunchFat = lunchFatDVC;
    pvc.lunchTotalCalories = lunchTotalCaloriesDVC;
    
    //Pass the dinner data to the DVC
    pvc.dinnerProtein = proteinD;
    pvc.dinnerCarbs = carbsD;
    pvc.dinnerFat = fatD;
    pvc.dinnerTotalCalories = totalCaloriesD;
    
    //Pass the goal values to the DVC
    pvc.goalCarbs = overallCarbsDVC;
    pvc.goalFat = overallFatDVC;
    pvc.goalProtein = overallProDVC;
    pvc.goalTotalCalories = overallCaloriesDVC;
    
    
}
@end