//
//  GoalViewController.h
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/8/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuViewController.h"
#import "CreateAccount.h"



@interface GoalViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *losingWeightB;
@property (strong, nonatomic) IBOutlet UIButton *maintainingWeightB;
@property (strong, nonatomic) IBOutlet UIButton *gainingWeightB;

- (IBAction)losingWeightButton:(id)sender;
- (IBAction)maintainingWeightButton:(id)sender;
- (IBAction)gainingWeightButton:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *infoTextView;

@property (strong, nonatomic) IBOutlet UIButton *viewProfileButton;
@property BOOL female;
@property NSString * firstname;


@end

