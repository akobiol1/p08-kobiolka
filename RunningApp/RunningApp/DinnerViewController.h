//
//  DinnerViewController.h
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/10/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuViewController.h"
#import "LViewController.h"



@interface DinnerViewController : UIViewController


@property int breakfastProteinDVC;
@property int breakfastCarbsDVC;
@property int breakfastFatDVC;
@property int breakfastTotalCaloriesDVC;

@property int lunchProteinDVC;
@property int lunchCarbsDVC;
@property int lunchFatDVC;
@property int lunchTotalCaloriesDVC;

@property int overallCaloriesDVC;
@property int overallProDVC;
@property int overallFatDVC;
@property int overallCarbsDVC;
@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet UIButton *button2;
@property (strong, nonatomic) IBOutlet UIButton *button3;
@property (strong, nonatomic) IBOutlet UIButton *button4;
@property (strong, nonatomic) IBOutlet UIButton *button5;
@property (strong, nonatomic) IBOutlet UIButton *button6;
@property (strong, nonatomic) IBOutlet UIButton *button7;
@property (strong, nonatomic) IBOutlet UIButton *button8;
@property (strong, nonatomic) IBOutlet UIButton *button9;
- (IBAction)omeletButton:(id)sender;
- (IBAction)veggieBurgerButton:(id)sender;
- (IBAction)smoothieButton:(id)sender;
- (IBAction)dinnerSaladButton:(id)sender;
- (IBAction)pastaButton:(id)sender;
- (IBAction)riceButton:(id)sender;
- (IBAction)subButton:(id)sender;
- (IBAction)macAndCheeseButton:(id)sender;
- (IBAction)fruitButton:(id)sender;

@end

