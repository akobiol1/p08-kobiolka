//
//  ViewController.m
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/2/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import "ViewController.h"
#import "MenuViewController.h"
#import "ProfileViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize enterNutritionButton, viewProfileButton, startARunButton, firstLoad, overallCaloriesVC, overallCarbsVC, overallFatVC, overallProVC, currentCarbsVC, currentCaloriesVC, currentFatVC, currentProVC;


- (void)viewDidLoad {
    
   // self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"greenBackground.jpg"]];

   /* enterNutritionButton.hidden = YES;
    viewProfileButton.hidden = YES;
    startARunButton.hidden = YES;*/
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)createAccountAction:(id)sender {
    
}

- (IBAction)enterNutritionAction:(id)sender {
    
    MenuViewController *mVC = [self.storyboard instantiateViewControllerWithIdentifier:@"menuVC"];
    [mVC setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:mVC animated:YES completion:nil];
   

    
    mVC.overallProMVC = overallProVC;
    mVC.overallCarbsMVC = overallCarbsVC;
    mVC.overallFatMVC = overallFatVC;
    mVC.overallCaloriesMVC = overallCaloriesVC;
    
}

- (IBAction)startARunAction:(id)sender {
    
}

- (IBAction)viewProfileAction:(id)sender {
    ProfileViewController *pVC = [self.storyboard instantiateViewControllerWithIdentifier:@"proVC"];
    [pVC setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:pVC animated:YES completion:nil];

    
    pVC.goalFat = overallFatVC;
    pVC.goalProtein = overallProVC;
    pVC.goalCarbs = overallCarbsVC;
    pVC.goalTotalCalories = overallCaloriesVC;
}

@end
