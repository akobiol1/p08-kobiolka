//
//  ProfileViewController.h
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/6/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface ProfileViewController : UIViewController


@property (strong, nonatomic) IBOutlet UILabel *proteinLabel;
@property (strong, nonatomic) IBOutlet UILabel *fatLabel;
 
 @property (strong, nonatomic) IBOutlet UILabel *caloriesLabel;

- (IBAction)returnHome:(id)sender;

@property NSInteger p;
@property NSInteger c;
@property NSInteger f;
@property NSInteger tc;

//stats for the boxes
@property NSInteger currentProtein;
@property NSInteger currentCarbs;
@property NSInteger currentFat;
@property NSInteger currentTotalCalories;

@property NSInteger goalProtein;
@property NSInteger goalFat;
@property NSInteger goalCarbs;
@property NSInteger goalTotalCalories;


//stats for meals
@property NSInteger breakfastProtein;
@property NSInteger breakfastCarbs;
@property NSInteger breakfastFat;
@property NSInteger breakfastTotalCalories;


@property NSInteger lunchProtein;
@property NSInteger lunchCarbs;
@property NSInteger lunchFat;
@property NSInteger lunchTotalCalories;


@property NSInteger dinnerProtein;
@property NSInteger dinnerCarbs;
@property NSInteger dinnerFat;
@property NSInteger dinnerTotalCalories;



@property (strong, nonatomic) IBOutlet UILabel *carbsLabel;


@property BOOL femalePVC;

@property (strong, nonatomic) IBOutlet UIImageView *imageView1;
@property (strong, nonatomic) IBOutlet UIImageView *imageView2;
@property (strong, nonatomic) IBOutlet UIImageView *imageView3;
@property (strong, nonatomic) IBOutlet UIImageView *imageView4;

@end

