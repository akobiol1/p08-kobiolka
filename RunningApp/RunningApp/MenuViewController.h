//
//  MenuViewController.h
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/5/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MenuViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *breakfast;

//used for passing goal information
@property int overallCaloriesMVC;
@property int overallProMVC;
@property int overallFatMVC;
@property int overallCarbsMVC;

@property (weak, nonatomic) IBOutlet UITextView *informationTextView;

- (IBAction)breakfastAction:(id)sender;


@end

