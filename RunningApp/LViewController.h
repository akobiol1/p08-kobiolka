//
//  LViewController.h
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/10/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BViewController.h"
#import "LViewController.h"



@interface LViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *button1;

@property (strong, nonatomic) IBOutlet UIButton *button2;
@property (strong, nonatomic) IBOutlet UIButton *button3;
@property (strong, nonatomic) IBOutlet UIButton *button4;
@property (strong, nonatomic) IBOutlet UIButton *button5;
@property (strong, nonatomic) IBOutlet UIButton *button6;
@property (strong, nonatomic) IBOutlet UIButton *button7;
@property (strong, nonatomic) IBOutlet UIButton *button8;
@property (strong, nonatomic) IBOutlet UIButton *button9;

- (IBAction)beanPastaButton:(id)sender;
- (IBAction)quinoaButton:(id)sender;
- (IBAction)hummusButton:(id)sender;
- (IBAction)saladButton:(id)sender;
- (IBAction)sandwichButton:(id)sender;
- (IBAction)soupButton:(id)sender;
- (IBAction)broccoliButton:(id)sender;
- (IBAction)appleButton:(id)sender;
- (IBAction)berriesButton:(id)sender;

@property int overallCaloriesLVC;
@property int overallProLVC;
@property int overallFatLVC;
@property int overallCarbsLVC;

@property int breakfastProteinLVC;
@property int breakfastCarbsLVC;
@property int breakfastFatLVC;
@property int breakfastTotalCaloriesLVC;

@end

