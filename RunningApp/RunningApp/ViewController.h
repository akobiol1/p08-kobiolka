//
//  ViewController.h
//  RunningApp
//
//  Created by Amanda Kobiolka on 5/2/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *enterNutritionButton;
@property (strong, nonatomic) IBOutlet UIButton *startARunButton;
@property (strong, nonatomic) IBOutlet UIButton *viewProfileButton;

@property BOOL firstLoad;

@property int currentCaloriesVC;
@property int currentProVC;
@property int currentFatVC;
@property int currentCarbsVC;


@property int overallCaloriesVC;
@property int overallProVC;
@property int overallFatVC;
@property int overallCarbsVC;

- (IBAction)createAccountAction:(id)sender;
- (IBAction)enterNutritionAction:(id)sender;
- (IBAction)startARunAction:(id)sender;
- (IBAction)viewProfileAction:(id)sender;

@end

